<div class="nav">
	<div class="menu">
		<ul class="d-flex justify-content-center">
			<li>
				<a href="/home.php">
					<img src="./assets/images/svg/main-nav-tabs/home-blue.svg" alt="">
					<p>Home</p>
				</a>
			</li>
			<li class="active">
				<a href="/clients.php">
					<img src="./assets/images/svg/main-nav-tabs/clients-blue.svg" alt="">
					<p>Clients</p>
				</a>
			</li>
			<li>
				<a href="/assessment.php">
					<img src="./assets/images/svg/main-nav-tabs/assessments-blue.svg" alt="">
					<p>Assessment</p>
				</a>
			</li>
			<li>
				<a href="/plan.php">
					<img src="./assets/images/svg/main-nav-tabs/account-blue.svg" alt="">
					<p>Account</p>
				</a>
			</li>
		</ul>
	</div>
</div>