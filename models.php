<?php require('includes/header.php') ?>


<div id="main" class="modals">
	<div class="container-fluid">
		
		<div class="row">	
			<div class="col-md-8">
		
				<!-- Select Client Modal -->
				<div class="popup">
					<div class="popup-dialog" role="document">
						<div class="popup-content">
							<div class="popup-header">
								<div class="popup-title">
									<div class="d-flex w-100">
										<div class="col-md-1">
											<button class="btn-icon">
												<img src="./assets/images/new-user.png">
											</button>
										</div>
										<div class="col-md-10 text-center">
											<h3>Select Client</h3>
										</div>
										<div class="col-md-1">
											<button class="btn-icon">
												<img src="./assets/images/close.png">
											</button>
										</div>
									</div>
								</div>
								<div class="popup-search">
									<input type="text" id="search" class="form-control">
									<label for="search" class="search-label">
										<img src="./assets/images/svg/search-icon.svg" alt="">
										<span>Search</span>
									</label>
								</div>
							</div>
							<div class="popup-body p-0">
								<ul class="list-group">
									<li class="list-group-item">
										<strong>James Meth</strong>
										<span class="float-right">48 Year Old</span>
									</li>
									<li class="list-group-item">
										<strong>Major Beth</strong>
										<span class="float-right">48 Year Old</span>
									</li>
									<li class="list-group-item">
										<strong>Nelly Greg</strong>
										<span class="float-right">48 Year Old</span>
									</li>
									<li class="list-group-item">
										<strong>Sally Sames</strong>
										<span class="float-right">48 Year Old</span>
									</li>
									<li class="list-group-item">
										<strong>Harry Itchi</strong>
										<span class="float-right">48 Year Old</span>
									</li>

								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- // Select Client Modal -->
				
			</div>
		</div>

		<div class="modal-divider"></div>

		<div class="row">	
			<div class="col-md-8">
		
				<!-- Assessment(s) for Estelle Potter Modal -->
				<div class="popup">
					<div class="popup-dialog" role="document">
						<div class="popup-content">
							<div class="popup-header">
								<div class="popup-title">
									<div class="d-flex w-100">
										<div class="col-md-1">
											<button class="btn-icon">
												<img src="./assets/images/new-user.png">
											</button>
										</div>
										<div class="col-md-10 text-center">
											<h3>Assessment(s) for Estelle Potter</h3>
										</div>
										<div class="col-md-1">
											<button class="btn-icon">
												<img src="./assets/images/close.png">
											</button>
										</div>
									</div>
								</div>
							</div>
							<div class="popup-body p-0 pt-3">
								
								<div class="popup-search">
									<input type="text" id="search" class="form-control">
									<label for="search" class="search-label">
										<img src="./assets/images/svg/search-icon.svg" alt="">
										<span>Search</span>
									</label>
								</div>
								<ul class="list-group">
									<li class="list-group-item checked">
										<strong>EPDS</strong>
										<span>- Full Unabbreviated Assessment Name Here</span>
										<span class="float-right">
											<span class="check"></span>
										</span>
									</li>
									<li class="list-group-item">
										<strong>EPDA</strong>
										<span>- Full Unabbreviated Assessment Name Here</span>
										<span class="float-right">
											<span class="check"></span>
										</span>
									</li>
									<li class="list-group-item checked">
										<strong>PCSL-5</strong>
										<span>- Full Unabbreviated Assessment Name Here</span>
										<span class="float-right">
											<span class="check"></span>
										</span>
									</li>
									<li class="list-group-item checked">
										<strong>CSA-6</strong>
										<span>- Full Unabbreviated Assessment Name Here</span>
										<span class="float-right">
											<span class="check"></span>
										</span>
									</li>
								</ul>
								<div class="bg-white p-3">
									<div class="selected_requests">
										<a href="" class="href">
											<span>ORT</span>
											<span class="cross"><img src="./assets/images/cross.png" class="minus-top-2"></span>
										</a>
										<a href="" class="href">
											<span>K10</span>
											<span class="cross"><img src="./assets/images/cross.png" class="minus-top-2"></span>
										</a>
									</div>
									
								</div>
							</div>
							<div class="popup-footer text-right">
								<button class="btn-icon">
									<strong>ADMINISTER (2)</strong>
									<img src="./assets/images/svg/start.svg" alt="">
								</button>
							</div>
						</div>
					</div>
				</div>
				<!-- // Assessment(s) for Estelle Potter Modal -->
				
			</div>
		</div>

		<div class="modal-divider"></div>

		<div class="row">	
			<div class="col-md-8">

				<!-- Future Logs Modal -->
				<div class="popup">
					<div class="popup-dialog" role="document">
						<div class="popup-content">
							<div class="popup-header">
								<div class="popup-title">
									<div class="d-flex w-100">
										<div class="col-md-1"></div>
										<div class="col-md-10 text-center">
											<h3>Future Logs</h3>
										</div>
										<div class="col-md-1">
											<button class="btn-icon">
												<img src="./assets/images/close.png">
											</button>
										</div>
									</div>
								</div>
							</div>
							<div class="popup-body">
								<ul class="list-group">
									<li class="list-group-item">
										<strong>ECR-S</strong>
										<span class="float-right">
											<span class="text-muted">Scheduled for</span>
											<span class="date">21 Jan 2017</span>
										</span>
									</li>
									<li class="list-group-item">
										<strong>ECR-S</strong>
										<span class="float-right">
											<span class="text-muted">Scheduled for</span>
											<span class="date">21 Jan 2017</span>
										</span>
									</li>
									<li class="list-group-item">
										<strong>ECR-S</strong>
										<span class="float-right">
											<span class="text-muted">Scheduled for</span>
											<span class="date">21 Jan 2017</span>
										</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- // Futire Logs Modal -->
				
			</div>
		</div>

		<div class="modal-divider"></div>

		<div class="row">	
			<div class="col-md-8">

				<!-- Sent Logs Modal -->
				<div class="popup">
					<div class="popup-dialog" role="document">
						<div class="popup-content">
							<div class="popup-header">
								<div class="popup-title">
									<div class="d-flex w-100">
										<div class="col-md-1"></div>
										<div class="col-md-10 text-center">
											<h3>Sent Logs</h3>
										</div>
										<div class="col-md-1">
											<button class="btn-icon">
												<img src="./assets/images/close.png">
											</button>
										</div>
									</div>
								</div>
							</div>
							<div class="popup-body">
								<ul class="list-group">
									<li class="list-group-item">
										<strong>ACR-S</strong>
										<span class="float-right">
											<span class="text-muted">Sent on</span>
											<span class="date">21 Jan 2017</span>
										</span>
									</li>
									<li class="list-group-item">
										<strong>TES-S</strong>
										<span class="float-right">
											<span class="text-muted">Sent on</span>
											<span class="date">21 Jan 2017</span>
										</span>
									</li>
									<li class="list-group-item">
										<strong>P2S-S</strong>
										<span class="float-right">
											<span class="text-muted">Sent on</span>
											<span class="date">21 Jan 2017</span>
										</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- // Sent Logs Modal -->
				
			</div>
		</div>

		<div class="modal-divider"></div>

		<div class="row">	
			<div class="col-md-8">
		
				<!-- Add Note Modal -->
				<div class="popup">
					<div class="popup-dialog" role="document">
						<div class="popup-content">
							<div class="popup-header">
								<div class="popup-title">
									<div class="d-flex w-100">
										<div class="col-md-1"></div>
										<div class="col-md-10 text-center">
											<h3>Add Note</h3>
										</div>
										<div class="col-md-1">
											<button class="btn-icon">
												<img src="./assets/images/close.png">
											</button>
										</div>
									</div>
								</div>
							</div>
							<div class="popup-body">
								<label for="note" class="text-muted small">Type Here</label>
								<textarea name="note" id="note" cols="30" rows="10" class="form-control"></textarea>
								<div class="note-btns text-right mt-3">
									<button class="btn-icon mr-3">
										<img src="./assets/images/svg/trash.svg" class="minus-top-2" alt="">
										<span>Delete Note</span>
									</button>
									<button class="btn btn-success">Add Note</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- // Add Note Modal -->
				
			</div>
		</div>

		<div class="modal-divider"></div>

		<div class="row">	
			<div class="col-md-8">
		
				<!-- Email to Estele Modal -->
				<div class="popup">
					<div class="popup-dialog" role="document">
						<div class="popup-content">
							<div class="popup-header">
								<div class="popup-title">
									<div class="d-flex w-100">
										<div class="col-md-1">
										</div>
										<div class="col-md-10 text-center">
											<h3>Email to Estele</h3>
											<p class="small mb-0">estellerpotter@gmail.com</p>
										</div>
										<div class="col-md-1">
											<button class="btn-icon">
												<img src="./assets/images/close.png">
											</button>
										</div>
									</div>
								</div>
							</div>
							<div class="popup-body p-0 pt-3">
								
								<div class="popup-search">
									<input type="text" id="search" class="form-control">
									<label for="search" class="search-label">
										<img src="./assets/images/svg/search-icon.svg" alt="">
										<span>Search</span>
									</label>
								</div>
								<ul class="list-group">
									<li class="list-group-item checked">
										<strong>EPDS</strong>
										<span>- Full Unabbreviated Assessment Name Here</span>
										<span class="float-right">
											<span class="check"></span>
										</span>
									</li>
									<li class="list-group-item">
										<strong>EPDA</strong>
										<span>- Full Unabbreviated Assessment Name Here</span>
										<span class="float-right">
											<span class="check"></span>
										</span>
									</li>
									<li class="list-group-item checked">
										<strong>PCSL-5</strong>
										<span>- Full Unabbreviated Assessment Name Here</span>
										<span class="float-right">
											<span class="check"></span>
										</span>
									</li>
									<li class="list-group-item checked">
										<strong>CSA-6</strong>
										<span>- Full Unabbreviated Assessment Name Here</span>
										<span class="float-right">
											<span class="check"></span>
										</span>
									</li>
								</ul>
								<div class="bg-white p-3">
									<div class="selected_requests">
										<a href="" class="href">
											<span>ORT</span>
											<span class="cross"><img src="./assets/images/cross.png" class="minus-top-2"></span>
										</a>
										<a href="" class="href">
											<span>K10</span>
											<span class="cross"><img src="./assets/images/cross.png" class="minus-top-2"></span>
										</a>
									</div>
									
								</div>
							</div>
							<div class="popup-footer text-right">
								<div class="float-left">
									<div class="attach_client">
										<div class="radio">
											<input type="checkbox" id="attach_client">
											<span class="checkbox-input"></span>
											<label for="attach_client">Email result to clients</label>
										</div>
									</div>
								</div>
								<button class="btn-icon">
									<strong>ADMINISTER (2)</strong>
									<img src="./assets/images/svg/start.svg" alt="">
								</button>
							</div>
						</div>
					</div>
				</div>
				<!-- // Email to Estele Modal -->
				
			</div>
		</div>

		<div class="modal-divider"></div>

		<div class="row">	
			<div class="col-md-8">
		
				<!-- Email to Estele Message Modal -->
				<div class="popup">
					<div class="popup-dialog" role="document">
						<div class="popup-content">
							<div class="popup-header">
								<div class="popup-title">
									<div class="d-flex w-100">
										<div class="col-md-1">
										</div>
										<div class="col-md-10 text-center">
											<h3>Email to Estele</h3>
											<p class="small mb-0">estellerpotter@gmail.com</p>
										</div>
										<div class="col-md-1">
											<button class="btn-icon">
												<img src="./assets/images/close.png">
											</button>
										</div>
									</div>
								</div>
							</div>
							<div class="popup-body pt-3">
								<div class="row">
									<div class="col">
										<div class="form-group">
											<label for="note" class="text-muted small">Subject</label>
											<input name="note" id="note" class="form-control">
										</div>
										<div class="form-group">
											<label for="note" class="text-muted small">Message</label>
											<textarea name="note" id="note" cols="30" rows="10" class="form-control"></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="popup-footer text-right">
								<div class="float-left">
									<div class="attach_client">
										<div class="radio">
											<input type="checkbox" id="attach_client2">
											<span class="checkbox-input"></span>
											<label for="attach_client2">Email result to clients</label>
										</div>
									</div>
								</div>
								<button class="btn-icon">
									<strong>ADMINISTER (2)</strong>
									<img src="./assets/images/svg/start.svg" alt="">
								</button>
							</div>
						</div>
					</div>
				</div>
				<!-- // Email to Estele Message Modal -->
			</div>
		</div>

		<div class="modal-divider"></div>

		<div class="row">	
			<div class="col-md-8">
		
				<!-- Schedule DASS-21 to Estelle Potter Modal -->
				<div class="popup">
					<div class="popup-dialog" role="document">
						<div class="popup-content schedule_setting">
							<div class="popup-header">
								<div class="popup-title">
									<div class="d-flex w-100">
										<div class="col-md-1">
										</div>
										<div class="col-md-10 text-center">
											<h3>Schedule DASS-21 to Estelle Potter</h3>
											<p class="small mb-0">estellerpotter@gmail.com</p>
										</div>
										<div class="col-md-1">
											<button class="btn-icon">
												<img src="./assets/images/close.png">
											</button>
										</div>
									</div>
								</div>
							</div>
							<div class="popup-body pt-3">
								<div class="row">
									<div class="col">
										<h4>Start On:</h4>
										<div class="form-group">
											<label for="" class="text-muted small">Select Date</label>
											<input type="text" class="form-control w-25">
										</div>

										<h4 class="mt-4">Repeat Every:</h4>
										<label for="" class="text-muted small">Select Frequency</label>
										<div class="radio-group">
											<div class="radio-input">
												<input type="radio" name="type" id="radio1" checked="checked">
												<label for="radio1">None</label>
											</div>
											<div class="radio-input">
												<input type="radio" name="type" id="radio2">
												<label for="radio2">Day </label>
											</div>
											<div class="radio-input">
												<input type="radio" name="type" id="radio3">
												<label for="radio3">Week</label>
											</div>
											<div class="radio-input">
												<input type="radio" name="type" id="radio4">
												<label for="radio4">Fortnight</label>
											</div>
											<div class="radio-input">
												<input type="radio" name="type" id="radio5">
												<label for="radio5">Month</label>
											</div>
											<div class="radio-input">
												<input type="radio" name="type" id="radio6">
												<label for="radio6">3 Months</label>
											</div>
										</div>

										<h4 class="mt-4">No of Repetitions:</h4>
										<label for="" class="text-muted small">Enter No(max 99)</label>
										<div>
											<input type="number" class="form-control w-25 float-left">
											<span class="d-inline-block p-2">concludes on <strong>Thu 21 June 2017</strong></span>
										</div>

									</div>
								</div>
							</div>
							<div class="popup-footer text-right">
								<div class="float-left">
									<div class="attach_client mt-1">
										<div class="radio">
											<input type="checkbox" id="attach_client2">
											<span class="checkbox-input"></span>
											<label for="attach_client2">Email result to clients</label>
										</div>
									</div>
								</div>
								<div class="note-btns text-right mt-0">
									<button class="btn-icon mr-3">
										<img src="./assets/images/svg/trash.svg" class="minus-top-2" alt="">
										<span>Delete Note</span>
									</button>
									<button class="btn btn-success">Add Note</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- // Schedule DASS-21 to Estelle Potter Modal -->
				
			</div>
		</div>

		<div class="modal-divider"></div>

		<div class="row">	
			<div class="col-md-8">
		
				<!-- Add Client Modal -->
				<div class="popup">
					<div class="popup-dialog" role="document">
						<div class="popup-content schedule_setting">
							<div class="popup-header">
								<div class="popup-title">
									<div class="d-flex w-100">
										<div class="col-md-1">
											<button class="btn-icon">
												<img src="./assets/images/left-arrow.png">
											</button>
										</div>
										<div class="col-md-10 text-center">
											<h3>Client Details</h3>
										</div>
										<div class="col-md-1">
											<button class="btn-icon">
												<img src="./assets/images/close.png">
											</button>
										</div>
									</div>
								</div>
							</div>
							<div class="popup-body pt-3">
								<div class="row">
									<div class="col">
										<div class="form-group">
											<label for="" class="text-muted small">First Name or ID</label>
											<input type="text" class="form-control">
										</div>
									</div>
									<div class="col">
										<div class="form-group">
											<label for="" class="text-muted small">Last Name or ID</label>
											<input type="text" class="form-control">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col">
										<div class="form-group">
											<label for="" class="text-muted small">Email</label>
											<input type="text" class="form-control">
										</div>
									</div>
									<div class="col">
										<div class="form-group">
											<label for="" class="text-muted small">Date of Birth</label>
											<input type="text" class="form-control">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col">
										<label for="" class="text-muted small">Select Frequency</label>
										<div class="radio-group">
											<div class="radio-input bg-white">
												<input type="radio" name="type" id="gender1" checked="checked">
												<label for="gender1">Undefined</label>
											</div>
											<div class="radio-input bg-white">
												<input type="radio" name="type" id="gender2">
												<label for="gender2">Male</label>
											</div>
											<div class="radio-input bg-white">
												<input type="radio" name="type" id="gender3">
												<label for="gender3">Female</label>
											</div>
										</div>

									</div>
								</div>
							</div>
							<div class="popup-footer text-center">
								<div class="note-btns text-center mt-0">
									<button class="btn-icon mr-3">
										<span>Add Client</span>
										<img src="./assets/images/svg/start.svg" class="minus-top-2" alt="">
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- // Add Client Modal -->
				
			</div>
		</div>
		
		<div class="modal-divider"></div>

		<div class="row">
			<div class="col-md-4">
				<div class="popup popup-confirmed">
					<div class="popup-dialog" role="document">
						<div class="popup-content schedule_setting">
							<div class="popup-body text-center pt-3">
								<img src="/assets/images/svg/tick.svg" class="mb-3" style="width:75px;" alt="">
								<p class="mb-0 mt-2">ORT, K10 Sent to</p>
								<h3 class="mt-0">Estelle Porte</h3>
							</div>
							<div class="popup-footer text-right">
								<button class="btn">OK</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="popup popup-confirmed">
					<div class="popup-dialog" role="document">
						<div class="popup-content schedule_setting">
							<div class="popup-body text-center pt-3">
								<img src="/assets/images/svg/tick.svg" class="mb-3" style="width:75px;" alt="">
								<h3 class="mt-0">Schedule Set</h3>
								<p class="text-muted mb-0 mt-2">Repeated DASS-21 scheduled on 21 Jul 2017 every week for 8 cycles to estellepotter@gmail.com</p>
							</div>
							<div class="popup-footer text-right">
								<button class="btn">OK</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


	</div>
</div>