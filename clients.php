<!DOCTYPE html>
<html lang="en">
	<head>
		<title>NovoPsych</title>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
		<link rel="stylesheet" href="../styles/styles.css"/>
	</head>
	<body>

<div id="main" class="inner-page clients-page">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 bg-white">
				<div class="inner-page-nav row">
						<div class="inner-page-header w-100 p-2">
							<h4 class="text-center">Clients</h4>
							<div class="inner-page-search">
								<input type="text" id="search" class="form-control">
								<label for="search" class="search-label">
									<img src="./assets/images/svg/search-icon.svg" alt="">
									<span>Search</span>
								</label>
							</div>
						</div>

						<div class="inner-page-list w-100">
							<div class="li-block">
								<p class="mb-1">Jenny Sabe</p>
								<p class="small">69 years old</p>
							</div>
							<div class="li-block">
								<p class="mb-1">Hena Bennett</p>
								<p class="small">27 years old</p>
							</div>
							<div class="li-block active">
								<p class="mb-1">Joshua Grant</p>
								<p class="small">52 years old</p>
							</div>
							<div class="li-block">
								<p class="mb-1">Sonu Sethi</p>
								<p class="small">43 years old</p>
							</div>
							<div class="li-block">
								<p class="mb-1">Matt Clinton</p>
								<p class="small">32 years old</p>
							</div>
							<div class="li-block">
								<p class="mb-1">Jakel Modi</p>
								<p class="small">38 years old</p>
							</div>
							<div class="li-block">
								<p class="mb-1">Rekha Putin</p>
								<p class="small">24 years old</p>
							</div>
							<div class="li-block">
								<p class="mb-1">Cristen Putin</p>
								<p class="small">21 years old</p>
							</div>

						</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="row">
					<div class="inner-page-content common-assessment-content w-100">
						<div class="single-client-page">
							<div class="client-info-block mt-4">
								<div class="client-info">
									<div class="row">
										<div class="col-md-8">
											<img src="./assets/images/none.png" class="float-left m-3 mt-4" alt="" style="width:70px;height:70px;">
											<div class="client-details mt-3">
												<h3 class="font-large font-weight-bold">Josua Grant</h3>
												<p class="mb-0">
													<span class="font-weight-bold">Age: </span>
													<span class="value"> 53 (7 July 1964)</span>
												</p>
												<p class="mb-0">
													<span class="font-weight-bold">Email: </span>
													<span class="value"> joshuagrant@email.com</span>
												</p>
											</div>
										</div>
										<div class="col-md-4 text-right">
											<img src="./assets/images/svg/edit.svg" alt="" class="m-2">
											<br/>
											<button class="btn btn-primary mr-3 mt-3 px-4">
												<img src="./assets/images/svg/ipad.svg" alt="">
												Administer
											</button>
										</div>
									</div>
								</div>
								<div class="client-actions">
									<div class="row">
										<div class="col-md-6 pr-0 text-center">
												<button class="text-center w-100">
													<img class="minus-top-2" src="./assets/images/svg/email-black.svg" alt="">
													Email
												</button>
										</div>
										<div class="col-md-6 pl-0 text-center">
											<button class="text-center w-100">
												<img class="minus-top-2" src="./assets/images/svg/email-black.svg" alt="">
												Subscribe
											</button>
										</div>
									</div>
								</div>
							</div>
							<div class="scheduling mt-4">
								<div class="header">
									<h3 class="float-left font-lead m-0">Scheduling</h3>
									<div class="scheduling-nav float-right">
										<a href="">View Sent Logs</a>
										<a href="">View Future Logs</a>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="scheduling-list">
									<div class="single-scheduling">
										<div class="status mb-2">
											<span class="signal success"></span>
											<span class="font-weight-bold">In Progress</span>
											<img src="./assets/images/svg/edit.svg" alt="" class="float-right">
											<!-- <span class="signal error"></span>
											<span class="signal warning"></span> -->
										</div>
										<p class="info">
											Repeated <span class="highlight">DASS-21</span> scheduled on <span class="highlight">21 July 2017</span> every <span class="highlight">Week</span> for <span class="highlight">8</span> cycles to <span class="highlight">emailtest@email.com</span>
										</p>
									</div>
									<div class="single-scheduling">
										<div class="status mb-2">
											<span class="signal error"></span>
											<span class="font-weight-bold">Expired</span>
											<img src="./assets/images/svg/edit.svg" alt="" class="float-right">
											<!-- <span class="signal error"></span>
											<span class="signal warning"></span> -->
										</div>
										<p class="info">
											Repeated <span class="highlight">DASS-21</span> scheduled on <span class="highlight">21 July 2017</span> every <span class="highlight">Week</span> for <span class="highlight">8</span> cycles to <span class="highlight">emailtest@email.com</span>
										</p>
									</div>
								</div>
							</div>

							<div class="past-results results mt-4">
								<div class="header">
									<h3 class="font-lead">Past Results</h3>
								</div>
								<ul class="list-group">
									<li class="list-group-item">
										<span class="date">21 JUNE 2017</span>
										<span class="code">DTC-1</span>
										<span class="message float-right">Contacted Via Email <img class="minus-top-2" src="./assets/images/svg/email-grey.svg" alt=""></span>
									</li>
									<li class="list-group-item">
										<span class="date">21 JUNE 2017</span>
										<span class="code">DTC-1</span>
										<span class="message float-right">Contacted in Person <img class="minus-top-2" src="./assets/images/svg/person-grey.svg" alt=""></span>
									</li>
									<li class="list-group-item">
										<span class="date">21 JUNE 2017</span>
										<span class="code">DTC-1</span>
										<span class="message float-right">Contacted Via Scheduled Email <img class="minus-top-2" src="./assets/images/svg/email-grey.svg" alt=""></span>
									</li>
									<li class="list-group-item">
										<span class="date">21 JUNE 2017</span>
										<span class="code">DTC-1</span>
										<span class="message float-right">Contacted Via Email <img class="minus-top-2" src="./assets/images/svg/email-grey.svg" alt=""></span>
									</li>
									<li class="list-group-item">
										<span class="date">21 JUNE 2017</span>
										<span class="code">DTC-1</span>
										<span class="message float-right">Contacted Via Email <img class="minus-top-2" src="./assets/images/svg/email-grey.svg" alt=""></span>
									</li>
								</ul>
							</div>

							<div class="past-results results mt-4">
								<div class="header">
									<h3 class="font-lead">Notes</h3>
								</div>
								<ul class="list-group">
									<li class="list-group-item">
										<span class="date">21 JUNE 2017</span>
										<span class="code">Lorem ipsum dolor sit amet, consectetur adipisicing.</span>
									</li>
									<li class="list-group-item">
										<span class="date">21 JUNE 2017</span>
										<span class="code">Lorem ipsum dolor sit amet, consectetur adipisicing.</span>
									</li>
									<li class="list-group-item">
										<span class="date">21 JUNE 2017</span>
										<span class="code">Lorem ipsum dolor sit amet, consectetur adipisicing.</span>
									</li>
									<li class="list-group-item">
										<span class="date">21 JUNE 2017</span>
										<span class="code">Lorem ipsum dolor sit amet, consectetur adipisicing.</span>
									</li>
									<li class="list-group-item">
										<span class="date">21 JUNE 2017</span>
										<span class="code">Lorem ipsum dolor sit amet, consectetur adipisicing.</span>
									</li>
								</ul>
								<button class="btn btn-primary mt-2 px-4">Add Note</button>
								<div class="pad30"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include('includes/navigation.php') ?>