<?php include('includes/header.php') ?>

<div id="main" class="inner-page single-assessment-page">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4">
				<div class="inner-page-nav assessment-nav row">
						<div class="inner-page-header assessment-header w-100 p-2">
							<h4 class="text-center">Assessments</h4>
							<div class="inner-page-search assessment-search">
								<input type="text" id="search" class="form-control">
								<label for="search" class="search-label">
									<img src="./assets/images/svg/search-icon.svg" alt="">
									<span>Search</span>
								</span>
							</div>
						</div>

						<div class="inner-page-list assessment-list w-100">
							<div class="li-block common-assessments">
								<p class="font-weight-bold mb-1">Common Assessment</p>
								<p class="small">Quick Access to common tests</span>
							</div>
							<div class="li-block">
								<p class="mb-1">USB-1</p>
								<p class="small">Unabbreviated Full Title of Form</span>
							</div>
							<div class="li-block">
								<p class="mb-1">USB-1</p>
								<p class="small">Unabbreviated Full Title of Form</span>
							</div>
							<div class="li-block active">
								<p class="mb-1">USB-1</p>
								<p class="small">Unabbreviated Full Title of Form</span>
							</div>
							<div class="li-block">
								<p class="mb-1">USB-1</p>
								<p class="small">Unabbreviated Full Title of Form</span>
							</div>
							<div class="li-block">
								<p class="mb-1">USB-1</p>
								<p class="small">Unabbreviated Full Title of Form</span>
							</div>
							<div class="li-block">
								<p class="mb-1">USB-1</p>
								<p class="small">Unabbreviated Full Title of Form</span>
							</div>
							<div class="li-block">
								<p class="mb-1">USB-1</p>
								<p class="small">Unabbreviated Full Title of Form</span>
							</div>
							<div class="li-block">
								<p class="mb-1">USB-1</p>
								<p class="small">Unabbreviated Full Title of Form</span>
							</div>

						</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="row">
					<div class="inner-page-content common-assessment-content w-100">

						<div class="assessment-info-block mt-4">
							<div class="assessment-info">
								<div class="row">
									<div class="col-md-8">
										<img src="./assets/images/svg/test-card-blue.svg" class="float-left m-3" alt="">
										<div class="assessment-details mt-3">
											<h3 class="font-large font-weight-bold">Depression Anxiety Stress Scales (Short Form)</h3>
										</div>
									</div>
									<div class="col-md-4 mt-4 text-right">
										<button class="btn btn-primary mr-3 mt-5 px-4">
											<img src="./assets/images/svg/ipad.svg" class="minus-top-2" alt="">
											Administer
										</button>
									</div>
								</div>
							</div>
							<div class="assessment-actions">
								<div class="row">
									<div class="col-md-6 pr-0 text-center">
											<button class="text-center w-100">
												<img class="minus-top-2" src="./assets/images/svg/email-black.svg" alt="">
												Email
											</button>
									</div>
									<div class="col-md-6 pl-0 text-center">
										<button class="text-center w-100">
												<img class="minus-top-2" src="./assets/images/svg/email-black.svg" alt="">
											Subscribe
										</button>
									</div>
								</div>
							</div>
						</div>
						<div class="single-assessment-details">
							<h4>Deep Fried Turkey Practice</h4>
							<p>Whenever you need to buy something that will go with whatever you are wearing that day, you may need to think about getting some designer watches. A watch is a piece of jewelry that you can wear every day, and it pretty much, will go with anything you are wearing. It’s not always going to make you look dressed up, but it may help you look more stylish, and sometimes that is all you want. Watches are the perfect answer to what you can wear with anything, because we are living in a world that always has to go through what time it is. </p>

							<h4>Bbq Grilling Versus Smoking The Great Debate</h4>
							<p>‘With this ring, I thee wed’. The most beautiful words ever spoken. The words that bring a man and woman together in a bond of eternity. This is done by slipping a ring on the bride’s and groom’s fingers by each other.  The wedding ring plays a most important role in bringing two hearts together in love for a lifetime.</p>

							<h4>Healthy Cooking Is A Must For Families</h4>
							<p>Do you feel that you will learn enough from this article to help you out with the subject matter at hand?</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include('includes/navigation.php') ?>