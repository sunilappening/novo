<?php include('includes/header.php') ?>

<div id="main" class="inner-page account-page">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 bg-white">
				<?php include('includes/account-nav.php') ?>
			</div>
			<div class="col-md-8">
				<div class="row">
					<div class="inner-page-content common-assessment-content w-100">
						<div class="page-header">
							<h3 class="font-lead m-0">Feedback</h3>
						</div>
						<div class="assessments">
							<div class="row">
								<div class="col">
									<form action="">
										<div class="form-group">
											<label for="">Feedback Type</label>
											<div class="radio-group">
												<div class="radio-input">
													<input type="radio" name="type" id="radio1" checked="checked">
													<label for="radio1">General</label>
												</div>
												<div class="radio-input">
													<input type="radio" name="type" id="radio2">
													<label for="radio2">Report an Error</label>
												</div>
												<div class="radio-input">
													<input type="radio" name="type" id="radio3">
													<label for="radio3">Feature Request</label>
												</div>
												<div class="radio-input">
													<input type="radio" name="type" id="radio4">
													<label for="radio4">Other</label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label for="">Type Message Here</label>
											<textarea type="text" class="form-control" rows="8"></textarea>
										</div>
										<div class="form-group">
											<button class="btn btn-success">Send</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include('includes/navigation.php') ?>